//console.log("Hello world");

//An array in programming is simply a list of data.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

let studentNumbers = ["2020-1923", "2020-1924" , "2020-1925", "2020-1926", "2020-1927"];

//Arrays
/*
	Arrays are used to store multiple values in a single variable
	They are declared using square brackets ([]) also known as "Array Literals"

	Syntax:
	let/const arrayName = [elemenA, elementB, elementC...]
*/

//Common examples of arrays

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox' , 'Gateway', 'Toshiba', 'Fujitsu'];

console.log(grades);
console.log(computerBrands);

//Possible use of an array but it is not recommended

let mixedArr = [12, 'Asus', null, undefined,{}];
console.log(mixedArr);

let myTasks = [
'drink html',
'eat javascript',
'inhales css',
'bake sass'

];
console.log(myTasks);

let tasks = ['feed the cats', 'wash the dishes', 'water the plants', "feed the birds", 'buy groceries'];

let capitalCities = ["Moscow", "Berlin", 'Tokyo', 'Manila'];

console.log(tasks);
console.log(capitalCities);

let city1 = "Tokyo";
let city2 = "Moscow";
let city3 = 'Berlin';

let cities = [city1,city2,city3];
console.log(cities);

//length property
//The .length property allows us to get and set the total number of items in an array.

console.log(myTasks.length);//4
console.log(cities.length);//3

let blankArr = [];
console.log(blankArr.length)//0

//length property can also be used with strings
//some array methods and properties can also be used with strings

let fullName = 'Jaime Noble';
console.log(fullName.length);

//length property can also set the total number of items in an array, meaning WE CAN ACTUALLT DELETE THE LAST ITEM IN THE ARRAY or SHORTEN the array by simply updating the length property of an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks); // nagbawas sa huli no. 4



//another example using decrementation
cities.length--;
console.log(cities);

fullName.length = fullName.length-1;
console.log(fullName.length);

fullName.length--;
console.log(fullName);

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
//theBeatles.length++;

theBeatles[4]="Cardo";
theBeatles[theBeatles.length]="Ely";
theBeatles[theBeatles.length]="Chito";
theBeatles[theBeatles.length]="MJ";

console.log(theBeatles);

let theTrainers = ['Ash']

function addTrainers(num){
	theTrainers[theTrainers.length]=num;
}
addTrainers("Brock");
addTrainers("Misty");
console.log(theTrainers);

//Reading from arrays
/* Accessing array elements is one of the more common task that we do with an array
-THis can be done through the use of aray indexes
-Each element in an array is associated with its own index/number

-the reason an array start with 0 is due to how the language is designed

Syntax 

	arrayName[index];

	*/

	console.log(grades[0]);//98.5
	console.log(computerBrands[3]);//Neo
//accessing an array element that does not exist will return 'Undefined'
	console.log(grades[20]);//undefined


let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];

console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];//Bron
console.log(currentLaker);
console.log(currentLaker);

console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2]="Pau Gasol";
console.log("Array after reassignment")
console.log(lakersLegends);

function findBlackMamba(mamba){
	return lakersLegends[mamba];
}
let blackMamba = findBlackMamba(0);
console.log(blackMamba);

let favoriteFoods = ["Tonkatsu", 'Adobo', 'Hamburger', 'Sinigang', 'Pizza'];
console.log(favoriteFoods);

favoriteFoods[3]="Sisig";
favoriteFoods[4]='Bopis';
console.log(favoriteFoods);


let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(lastElementIndex);//4
console.log(bullsLegends.length);//5

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

//adding items into the array

let newArr = [];
console.log(newArr[0])

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]); //undefined
newArr[1] = 'Tifa Lockhart';
console.log(newArr);

//newArr[newArr.length-1] = "Aerith Gainsborough"; // will replace tifa
newArr[newArr.length] = 'Barret Wallace'; //dadagdag si barret
console.log(newArr);

//Looping over an array

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}


let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5")
	}else{
		console.log(numArr[index] + " is not divisible by 5");
}

//MultiDimensional Arrays

/*
	-Multidimensional Arrays are useful for storing complex data structures
	-a practical application of this is to help visualize/create real world objects
*/

let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8']
];

console.log(chessBoard);

console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[1][5]);//f2

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);
}